<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Kata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $kata = "Predis atau redis digunakan untuk melakukan server caching pada Laravel";
        $arr_kata = explode(' ', $kata);
        foreach($arr_kata as $ark){
            DB::table('belajar_redis')->insert([
                'item' => $ark
            ]);
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}

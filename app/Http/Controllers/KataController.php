<?php

namespace App\Http\Controllers;

use App\Kata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class KataController extends Controller
{
    public function withCache(){
        $query = Cache::remember("kata_all", 10 * 60, function(){
            return Kata::all();
        });

        foreach($query as $q){
            echo "<li>{$q->item}</li>";
        }
    }

    

    public function withQuery()
    {
        $query = Kata::all();
        foreach($query as $q){
            echo "<li>{$q->item}</li>";
        }
    }
}
